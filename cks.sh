#!/bin/bash
# Looping through OCI servers using short hostnames, listing kernel paramters then listing only what we care about using egrep
domain='contoso.com'
for hostname in $(cat servers2.txt);
do ssh -x $hostname.$domain "hostname;
# the --ignore 2>&1 suppresses the sysctl: reading key "net.ipv6.conf.all.stable_secret" warnings
sysctl -a --ignore 2>&1 |egrep 'kernel.msgmnb|kernel.msgmni|kernel.msgmax|kernel.shmmax|kernel.shmall|kernel.core_uses_pid|net.ipv4.tcp_keepalive_time|net.ipv4.tcp_timestamps|net.ipv4.tcp_window_scaling|net.ipv4.ip_local_port_range';
ulimit -n";done
